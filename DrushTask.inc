<?php

/**
 * @file
 * Drush Task object definition.
 */

/**
 * Class representing a 'task'.
 *
 * Includes setup config and result storage.
 */
class DrushTask {

  /**
   * Drush site alias to target.
   *
   * @var string
   */
  public $site = '@self';
  /**
   * The basic drush command.
   *
   * @var string
   */
  public $command = 'status';
  /**
   * Arguments for the command command.
   *
   * @var string
   */
  public $arguments = '';
  /**
   * The drush output-format parameter.
   *
   * There are times when JSON would be preferable,
   * but for now we expect and parse normal text responses.
   * It can be overridden though.
   *
   * @var string
   * @see `drush topic docs-output-formats`
   */
  public $format = '';
  /**
   * The full commandline that was executed, after being wrapped in handlers.
   *
   * @var string
   */
  public $commandRaw = '';
  /**
   * Response code from running the command.
   *
   * 0 is success, anything else is an error code.
   *
   * @var int
   */
  public $resultCode = NULL;
  /**
   * Full unparsed text of the output.
   *
   * Expected to be JSON, but if there was an error, may be just text.
   *
   * @var text
   */
  public $resultRaw = '';
  /**
   * Full commandline output.
   *
   * @var text
   */
  public $resultText = '';
  /**
   * The data structure (unpacked json) result from a successful command run.
   *
   * @var text
   */
  public $result = '';
  /**
   * Configurations. The contents of variable_get('drush_task');
   *
   * @var array
   */
  private $settings = array();
  /**
   * How much diagnostics to show to the screen.
   *
   * @var int
   */
  private $verbosity = 0;

  const ERROR_TIMEOUT = 1999;
  const ERROR_FILE_NOT_FOUND = 6;

  /**
   * Construct a task definition.
   *
   * @inheritdoc
   */
  public function __construct($command, $arguments = NULL, $site = NULL) {
    $this->command = $command;
    $this->arguments = $arguments;
    $this->site = $site;
    // I guess one day this becomes OO injections of some type.
    // Until then...
    $this->settings = array_merge(drush_task_default_settings(), variable_get('drush_task', array()));
  }

  /**
   * Initialize configuration values.
   *
   * @see drush_task_default_settings()
   */
  public function setSettings($settings) {
    $this->settings = $settings;
  }

  /**
   * Evaluate the command on the cli.
   *
   * Return the result as a decoded JSON struct if successful.
   * If the return is === NULL
   * (check the type if there is a chance of a valid empty return)
   * then you should investigate the $task->resultCode and
   * $task->resultRaw yourself.
   */
  public function run() {

    // Assert that the provided settings are useful.
    // This is just an early sanity-check to protect ourselves from making mad
    // mistakes on the commandline.
    if (empty($this->settings['drush_path']) || !file_exists($this->settings['drush_path'])) {
      $strings = array(
        '!settings' => l(t('Web Services: Drush task settings'), 'admin/config/services/drush_task'),
      );
      // Use PHP error triggers here, as it's pretty serious, needs logging,
      // and we don't know what context we are being called from.
      // Do not assume page rendering etc, could be cron, could be web service.
      trigger_error(t('!settings do not contain a valid drush_path. This is required.', $strings), E_USER_ERROR);
      $this->resultCode = $this::ERROR_FILE_NOT_FOUND;
      return NULL;
    }

    $strings = array();
    // Review very carefully for security back doors.
    // All administrator-user-supplied strings should be run through
    // escapeshellarg() everywhere.
    //
    // I break the $exec_[prefix,command,suffix] into slices
    // just so the reports are not cluttered with backend irrelevancies.
    //
    // Prefix sets up environment vars that may be necessary due to CLI issues.
    $exec_prefix = '';
    // Exec is the path to drush and possible drushrc and globals.
    $drush_exec = '';
    // Command is the actual drush command and arguments.
    $drush_command = '';
    $exec_suffix = '';

    // The PHP runtime configuration parameter can confuse the system
    // about which version of PHP it's using.
    // Unsetting it seems to help.
    // $exec_prefix .= "PHPRC= ";.
    // If you need to use a specific PHP, the DRUSH_PHP env var should help.
    // Just running drush with an explict php path may not has the expected
    // result, as drush opens up its own sub threads later.
    if (!empty($this->settings['drush_php'])) {
      $exec_prefix .= ' DRUSH_PHP=' . escapeshellarg($this->settings['drush_php']) . ' ';
    }

    // The drush core part.
    $drush_exec .= escapeshellarg($this->settings['drush_path']) . ' ';
    if (!empty($this->settings['drush_rc'])) {
      $drush_exec .= ' --config=' . escapeshellarg($this->settings['drush_rc']) . ' ';
    }
    if (!empty($this->format)) {
      // Backend is a whole different story. Wonderfully verbose,
      // but needs tricky un-parsing. drush->drush specific really.
      // $exec .= ' --backend ';
      // We just act as a naiive client asking for data via json.
      $drush_exec .= ' --format=' . escapeshellarg($this->format) . ' ';
    }
    $strings['@site'] = '';
    if (!empty($this->site)) {
      $drush_exec .= escapeshellarg($this->site) . ' ';
      $strings['@site'] = " (on {$this->site})";
    }

    // The command that drush runs.
    $drush_command .= escapeshellarg($this->command) . ' ';

    if (!empty($this->arguments)) {
      // For safer escaping, split all the input string into bits,
      // (while respecting quoted values)
      // so the parts can be quoted and escaped individually.
      // str_getcsv() is a handy quote-safe string splitter.
      $safely_split = str_getcsv(trim($this->arguments), ' ');
      $safely_escaped = array_map('escapeshellarg', $safely_split);
      $drush_command .= implode(' ', $safely_escaped) . ' ';
    }

    // Capture all stdout and stderr in one stream.
    $exec_suffix .= ' 2>&1';
    // Apply a timeout.
    $time = 10;
    $resultCode = NULL;

    // TODO - continue to apply  all sorts of security paranoia here.
    $this->commandRaw = "$exec_prefix $drush_exec $drush_command $exec_suffix";
    $strings['%exec_command'] = $drush_command;
    $strings['%commandRaw'] = $this->commandRaw;

    try {
      watchdog(__CLASS__, "Calling `%exec_command`; @site", $strings, WATCHDOG_INFO);
      $result_array = array();

      switch ($this->settings['exec_method']) {

        case 'execute_timeout':
          $this->execute_timeout($this->commandRaw, $result_array, $resultCode, $time);
          break;

        case 'symfony/process':
          $this->exec_symfony($this->commandRaw, $result_array, $resultCode, $time);
          break;

        case exec:
        default:
          exec($this->commandRaw, $result_array, $resultCode);
          break;
      }

      $this->resultCode = $resultCode;
      $strings['!resultCode'] = ($resultCode) ? $resultCode . '[fail]' : '0:[success]';
      // If using exec_timeout, the result_array is already just one stream.
      if (is_array($result_array)) {
        $this->resultRaw = implode("\n", $result_array);
      }
      else {
        $this->resultRaw = $result_array;
      }


      if ($resultCode == $this::ERROR_TIMEOUT) {
        // Make noise about this, no matter what the verbosity, as it's the sort
        // of thing that emerges when conditions change after testing.
        $strings['@cmd'] = $this->commandRaw;
        $strings['@time'] = $time;
        $message = "Process timed out (given @time s) when running @cmd";
        watchdog(__FUNCTION__, $message, $strings, WATCHDOG_WARNING);
        // But be more coy about the details, to avoid unwanted detail leaking.
        if ($this->settings['verbosity'] >= 1) {
          // Full message to screen.
          drupal_set_message(t($message, $strings), 'error');
        }
        else {
          $strings['@cmd'] = 'drush_task';
          drupal_set_message(t($message, $strings), 'error');
          drupal_set_message(t('Details in the log (or enable drush_task debugging)'));
        }
      }

      $strings['!result'] = $this->resultRaw;

      $message = "drush_task called %exec_command; Result was <b>!resultCode</b> <pre>%commandRaw</pre> <pre>!result</pre>";
      watchdog(__CLASS__, $message, $strings, WATCHDOG_INFO);

      // $resultCode of 0 is good.
      if (!$resultCode) {
        if ($this->format == 'json') {
          $this->result = json_decode($this->resultRaw);
        }
        else {
          $this->result = $this->resultRaw;
          $this->resultText = trim($this->resultRaw);
        }
        // Log to screen if verbosity is high.
        if ($this->settings['verbosity'] >= 2) {
          // Full message to screen.
          drupal_set_message(t($message, $strings), 'info');
        }
        return $this->result;
      }
      else {
        // Returning a resultcode meant there was some sort of problem.
        // Echo to screen for quick diagnosis.
        if ($this->settings['verbosity'] >= 1) {
          // Full message to screen.
          drupal_set_message(t($message, $strings), 'error');
        }
        // Note, sometimes drush returning 'errors' is an expected effect,
        // depending what the question was - so don't automatically assume it's
        // a problem. The caller function should take it from here.
      }
    }
    catch (Exception $e) {
      $strings['!error'] = $e->getMessage();
      $message = "Called %exec_command; Result was <b>!resultCode</b> <pre>%commandRaw</pre>Error was !error <pre>!result</pre>";
      watchdog(__CLASS__, $message, $strings, WATCHDOG_ERROR);
      if ($this->settings['verbosity'] >= 1) {
        // Full message to screen.
        drupal_set_message(t($message, $strings), 'error');
      }
    }

    return NULL;
  }

  /**
   * Execute a command and return its output - with timeout.
   *
   * Either wait until the command exits or the timeout has expired.
   * Tries to return results and maintain a similar signature to core
   * exec().
   *
   * https://stackoverflow.com/a/13287902
   *
   * @param $cmd
   * @param $stdout
   * @param $stderr
   * @param bool $timeout
   * @return int
   */
  function execute_timeout($cmd, &$stdout, &$resultCode, $timeout=10) {
    $exec_prefix = "PHPRC= COLUMNS=1000 ";

    $pipes = array();
    $process = proc_open(
      $exec_prefix . $cmd,
      array(array('pipe','r'),array('pipe','w'),array('pipe','w')),
      $pipes
    );
    $start = time();
    $stdout = '';
    $stderr = '';

    if(is_resource($process)) {
      stream_set_blocking($pipes[0], 0);
      stream_set_blocking($pipes[1], 0);
      stream_set_blocking($pipes[2], 0);
      fclose($pipes[0]);
    }

    while(is_resource($process)) {
      $stdout .= stream_get_contents($pipes[1]);
      $stderr .= stream_get_contents($pipes[2]);

      if($timeout !== false && time() - $start > $timeout) {
        proc_terminate($process, 9);
        $resultCode = $this::ERROR_TIMEOUT;
        return $this::ERROR_TIMEOUT;
      }

      $status = proc_get_status($process);
      if(!$status['running']) {
        fclose($pipes[1]);
        fclose($pipes[2]);
        proc_close($process);
        $resultCode = $status['exitcode'];
        return $status['exitcode'];
      }

      usleep(100000);
    }

    return 1;
  }

  /**
   * A version of exec that delegates to the symfony/process library.
   *
   * This promises to handle timeouts and cross-platform issues for us.
   *
   * @param $cmd
   * @param array $result_array
   * @param int $resultCode
   * @param int $time
   */
  public function exec_symfony($cmd, &$result_array = array(), &$resultCode = 0, $time = 10) {
    include_once __DIR__ . '/vendor/autoload.php';
    if (!class_exists('Symfony\Component\Process\Process')) {
      trigger_error('symfony/process library not found. You probably have to install the composer dependency first before using this utility');
      $resultCode = 1;
      return FALSE;
    }

    // The PHP runtime configuration parameter can confuse the system
    // about which version of PHP it's using.
    // Un-setting it seems to help.
    // Arbitrary word wrap in this context is unhelpful.
    $env = array(
      "PHPRC" => "",
      "COLUMNS" => 1000,
    );

    $process = new Symfony\Component\Process\Process($cmd);
    $process->setEnv($env);
    $process->setTimeout($time);
    try {
      $process->run();
      $resultCode = $process->getExitCode();
    }
    catch (\Symfony\Component\Process\Exception\ProcessTimedOutException $e) {
      // A timeout is expected, and we know how to deal with it.
      // Any other exceptions, let them bubble.
      $resultCode = $this::ERROR_TIMEOUT;
    }
    $result_array = [$process->getOutput()];
  }

}
