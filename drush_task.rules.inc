<?php

/**
 * @file
 * Rules declarations.
 */

/**
 * Implements hook_rules_action_info().
 *
 * This action takes up to three arguments,
 * drush command, drush args, and drush site-alias
 * and makes the result of the drush
 * call available as avaiable that later rules can operate on.
 */
function drush_task_rules_action_info() {
  return array(
    'drush_task_rules_action_run_drush_task' => array(
      'label' => t('Run Drush Task'),
      'group' => t('Service'),
      'parameter' => array(

        'command' => array(
          'type' => 'text',
          'restriction' => 'input',
          'label' => t('Command'),
          'description' => t('Drush command.'),
          'optional' => FALSE,
          'default value' => 'status',
          'allow null' => FALSE,
          'sanitize' => TRUE,
        ),

        'arguments' => array(
          'type' => 'text',
          'restriction' => 'input',
          'label' => t('Arguments'),
          'description' => t('Enter commandline options and arguments, space separated. Tokens are supported.'),
          'optional' => TRUE,
          'default value' => '',
          'allow null' => TRUE,
          'sanitize' => TRUE,
        ),

        'site_alias' => array(
          'type' => array('text', 'token'),
          'default mode' => 'input',
          'label' => t('Site Alias'),
          'description' => t('OPTIONAL. The Site Alias. If not set, will use the current site as default context.'),
          'optional' => TRUE,
          'default value' => '',
          'allow null' => TRUE,
        ),
      ),

      'provides' => array(
        'drushtask' => array(
          'label' => 'Task',
          'type' => 'drushtask',
        ),
      ),

      'callbacks' => array(
        'help' => 'drush_task_rules_action_run_drush_task_help',
      ),

    ),
  );
}

/**
 * Returns additional help text to show on the Rules config form.
 */
function drush_task_rules_action_run_drush_task_help() {
  // Go to enormous lengths to avoid repeating myself,
  // and use the introspected settings to describe my own parameters.
  $data_info = drush_task_rules_data_info();
  $struct_info = $data_info['drushtask'];
  $strings['@label'] = $struct_info['label'];
  $properties = array();
  foreach ($struct_info['property info'] as $key => $info) {
    $strings['%property_label'] = $info['label'];
    $strings['%property_type'] = $info['type'];
    $properties[$key] = t('%property_label (%property_type)', $strings);
  }

  $help = array();
  $help[] = array(
    '#markup' => t("
      <p>
      Running a drush command will evaluate the given instructions, and produce
      a @label <em>response</em> object, containing attributes.
      </p>", $strings),
  );
  $help[] = array(
    '#theme' => 'item_list',
    '#items' => $properties,
  );
  $help[] = array(
    '#markup' => t("
    <p>Though not required, best results are often obtained by calling drush
    with the --json flag. If this is done, then a keyed structure may be
    parsed from the result, and used as data for the rule.
    "),
  );
  $help[] = array(
    '#markup' => t("
      <p>
      If a drush command fails, it will return no text result into
      <kbd>[drushtask:result-text]</kbd>.
      Failure state can be checked with <kbd>[drushtask:success]</kbd>.
      The full error text may be available in the logs, or as
      <kbd>[drushtask:result-raw]</kbd>
      ", $strings),
  );
  return $help;
}

/**
 * Runs a drush command with the given values.
 *
 * An action callback.
 *
 * @param string $command
 *   Drush command.
 * @param string $arguments
 *   Drush arguments.
 * @param string $site_alias
 *   Optional drush site-alias or site identifier.
 * @param array $params
 *   Array of currently avaiable parameters.
 * @param RulesState $state
 *   Something about the action.
 * @param RulesAction $action
 *   Currently running rule context.
 * @param string $op
 *   Probably 'execute'.
 *
 * @return DrushTask
 *   A rules_action_execution_callback() .
 *
 * @see rules_action_execution_callback()
 */
function drush_task_rules_action_run_drush_task($command, $arguments, $site_alias, $params, RulesState $state, RulesAction $action, $op) {
  $task = new DrushTask($command, $arguments, $site_alias);

  // IF a site_alias is set, we should assume there is a chance of a timeout or
  // other long-running task.
  // Returning a response immediately is only possible when not using the queue.
  drupal_alter('drush_task_pre_run', $task, $state, $action);
  $task->run();
  drupal_alter('drush_task_post_run', $task, $state, $action);

  return array('drushtask' => $task);

  $response = array(
    'success' => empty($task->resultCode),
    'result_code' => $task->resultCode,
    'result_text' => $task->resultText,
    'result' => $task->result,
  );
  return array('drushtask' => $response);

  // TODO: queuing?
  // drush_task_enqueue($task);
}

/**
 * The result of running a drush_task is a DrushTask object.
 *
 * ...which can be inspected for results.
 * Here we list the attributes of the task object
 * that we wany Rules to be able to access.
 * They show up in the UI for inspection or mapping.
 *
 * For Rules, a DrushTask object contains attributes
 * ->success : Bool
 * ->result_code : Int
 * ->result : Struct
 * ->result_text : Text
 *
 * They should be provided in the response from the action.
 *
 * Implements hook_rules_data_info().
 *
 * @inheritdoc
 * @see hook_rules_data_info()
 */
function drush_task_rules_data_info() {
  return array(
    'drushtask' => array(
      'label' => t('Drush Task'),
      'group' => t('Service'),
      'wrap' => TRUE,
      'property info' => array(
        'success' => array(
          'label' => t('Success'),
          'type' => 'boolean',
          'getter callback' => 'drush_task_drushtask_get_success',
        ),
        'result_code' => array(
          'label' => t('Result code'),
          'type' => 'text',
          'getter callback' => 'drush_task_drushtask_get_result_code',
        ),
        'result' => array(
          'label' => t('Result data'),
          // As I can't reliably say what format this comes in, I can't
          // reliably map it to any other type as a value.
          // Can I get a universal donor?
          'type' => '*',
          'getter callback' => 'drush_task_drushtask_get_result',
        ),
        'result_text' => array(
          'label' => t('Result text'),
          'type' => 'text',
          'getter callback' => 'drush_task_drushtask_get_result_text',
        ),
        'result_raw' => array(
          'label' => t('Result raw'),
          'type' => 'text',
          'getter callback' => 'drush_task_drushtask_get_result_raw',
        ),
      ),

    ),
  );
}

/**
 * Getter callbacks for the values.
 *
 * Neccessary as Feeds prohibits camelCase,
 * but OO standards want it. Thus I can't use native getters.
 *
 * These are functions not methods, as the $task isn't a fully-formed
 * entity, so it doesn't have entity_metadata_wappers etc helping it.
 */
function drush_task_drushtask_get_success($task) {
  // Success is really just an easier way of saying 'result was no error'.
  return $task->resultCode == 0;
}

/**
 * Getter for task result.
 */
function drush_task_drushtask_get_result_code($task) {
  return $task->resultCode;
}

/**
 * Getter for task result.
 */
function drush_task_drushtask_get_result($task) {
  return $task->result;
}

/**
 * Getter for task result.
 */
function drush_task_drushtask_get_result_text($task) {
  return $task->resultText;
}

/**
 * Getter for task result.
 */
function drush_task_drushtask_get_result_raw($task) {
  return $task->resultRaw;
}
