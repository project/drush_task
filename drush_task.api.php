<?php

/**
 * @file
 * Documentation for drush_tasks hooks.
 */

/**
 * Allow modification of a task before it runs.
 *
 * @param DrushTask $task
 *   The task object, with the command already set.
 */
function hook_drush_task_pre_run_alter(DrushTask $task, RulesState $state = NULL, RulesAction $action = NULL) {
  $task->arguments .= " --verbose --simulate ";
  $task->timeout = 2;
}

/**
 * Allow modification of the response after a task runs.
 *
 * @param DrushTask $task
 *   The task object, the $result is probably populated.
 */
function hook_drush_task_post_run_alter(DrushTask $task, RulesState $state = NULL, RulesAction $action = NULL) {

}
