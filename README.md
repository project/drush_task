



## Security: Configuration lockdown

This module has the potential to introduce security risks, 
as it runs comands on the server.
Think hard about what that means to you, and how well you are protecting
your environment.

Giving users the ability to run arbitrary drush tasks on the system can be
pretty dangerous, as drush can destroy a site if you ask it to.

To limit the potential for mis-use,
[the administration UI](/admin/config/services/drush_task) 
is **locked down by default**
when you first install it.
System-level settings here will be **disabled**.

You **must** review these instructions first, then carefully choose 
to unlock it.

### Unlocking the admin UI

In your sites settings.php (or local.settings.php), add the line:

    $conf['drush_task_ui_unlocked'] = TRUE;
    
This will make the admin editable for you to set up paths for your system.
It's suggested that you set that value back to FALSE again once you are done
so that these values cannot be changed.

This flag is NOT managed through Drupal role permissions, as this tool really
must not be enabled on a live server unless you are a site manager
with write-access to settings.php.
Core does this with `$update_free_access` for similar reasons.

### Setting the drush options at the server level

Alternatively (or after setting it up once using the unlocked admin UI),
you can explicitly **set the options for your server in settings.php**.
This is most helpful if your development and production environments are different,
as it makes things continue working during a deployment.

This approach is also much more secure and recommended for a live environment.

You can set the following:

    $conf['drush_task_ui_unlocked'] = FALSE;
    // Note that the lock is of a different format - separate from the actual settings.
    $conf['drush_task']['drush_path'] = '/usr/local/bin/drush';
    $conf['drush_task']['drush_php'] = '';
    $conf['drush_task']['drush_rc'] = '';
    $conf['drush_task']['verbosity'] = 0;
    $conf['drush_task']['exec_method'] = 'execute_timeout';

For best security, you should probably set them all.
See drush_task_default_settings() for the current defaults.


## Running PHP - choose the exec method

Due to a wide range of possible environments, and ways that
PHP may be running on your webserver, a choice of ways that drush can be called is provided.
Details are on the [config page](/admin/config/services/drush_task).

### Use PHP Process management approach

This is a local attempt to work around timeout issues identified so far.
*Works on my machine* (Acquia Dev Desk) but has not been tested on more esoteric environments
 (Nginx, IIS).

### Install symfony/process component (optional)

Highly recommended is the [symfony/process](http://symfony.com/doc/current/components/process.html)
component. This has the potential to work better on Windows, and has better error handling.
 
However this requires an extra installation step.

Short version: if you are already using Composer, then you should move into the module directory
and run

    composer install
  
It's possible that the [Drupal Composer Manager](https://www.drupal.org/project/composer_manager) module
may help also, or if you are already using a composer-based workflow it may just work - untested.

If these options do not appeal to you, it's probably best to just not choose the symfony plugin method.

### Avoid PHP native exec

This is potentially likely to hang your webserver process if mis-configured,
as it spawns a system call that may not return (depending on the drush command)
but may be worth a try if neither of the other methods work for your setup.

Importantly, drush commands that require user confirmation will hang unless given the
`--yes` flag when you configure it.

### Use native Drush PHP calls

**wishlist**: It may be possible to bootstrap and execute Drush inside the running PHP process.
This is not going to be practical to develop in Drupal7

